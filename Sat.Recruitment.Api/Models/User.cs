﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Sat.Recruitment.Api.Models
{
    public class User
    {
        [Required(ErrorMessage = "The {0} is required")]
        public string Name { get; set; }
        [Required(ErrorMessage = "The {0} is required")]

        public string Email { get; set; }
        [Required(ErrorMessage = "The {0} is required")]

        public string Address { get; set; }
        [Required(ErrorMessage = "The {0} is required")]

        public string Phone { get; set; }
        [Required(ErrorMessage = "The {0} is required")]

        public string UserType { get; set; }
        [Required(ErrorMessage = "The {0} is required")]
        public decimal Money { get; set; }
    }
}
