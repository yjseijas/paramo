﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sat.Recruitment.Api.Models;
using Sat.Recruitment.Api.Services.IServices;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Sat.Recruitment.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public partial class UsersController : ControllerBase
    {
        private IUser _user;
        public UsersController(IUser user)
        {
            this._user = user;
        }

        [HttpPost]
        [Route("/create-user")]
        public async Task<Result> CreateUser(User userParam)    
        {
            return await this._user.CreateUser(userParam);
        }
    }
}
