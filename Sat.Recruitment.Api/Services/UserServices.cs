﻿using Sat.Recruitment.Api.Models;
using Sat.Recruitment.Api.Repository;
using Sat.Recruitment.Api.Services.IServices;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Sat.Recruitment.Api.Services
{
    public class UserServices : IUser

    {
        private readonly List<User> _users = new List<User>();

        public async Task<Result> CreateUser(User userParam)
        {
            var errors = "";
            var name = userParam.Name;
            var email = userParam.Email;
            var address = userParam.Address;
            var phone = userParam.Phone;
            var userType = userParam.UserType;
            var money = userParam.Money.ToString();

            ValidateErrors(name, email, address, phone, ref errors);

            if (errors != null && errors != "")
                return new Result()
                {
                    IsSuccess = false,
                    Errors = errors
                };
           
            var newUser = new User
            {
                Name = name,
                Email = email,
                Address = address,
                Phone = phone,
                UserType = userType,
                Money = ChangeMoney(decimal.Parse(money), userType)
            };

            FillList(newUser);

            try
            {
                var isDuplicated = CheckDuplicated(newUser);

                if (isDuplicated)
                {
                    Debug.WriteLine("The user is duplicated"); ;
                    return new Result()
                    {
                        IsSuccess = false,
                        Errors = "The user is duplicated"
                    };
                }

                Debug.WriteLine("User Created");
                return new Result()
                {
                    IsSuccess = true,
                    Errors = "User Created"
                };              
            }
            catch
            {
                Debug.WriteLine("Unexpected Error.");
                return new Result()
                {
                    IsSuccess = false,
                    Errors = "Unexpected Error."
                };
            }
        }

        private bool CheckDuplicated(User newUser)
        {
            var isDuplicated = ( _users.Any(user => user.Email == newUser.Email) || _users.Any(user => user.Phone == newUser.Phone )) ||
                               ( _users.Any(user => user.Name == newUser.Name) && _users.Any(user => user.Address == newUser.Address) );

            return isDuplicated;
        }
        private void FillList(User newUser)
        {
            var userReader = new UserReader();
            var reader = userReader.ReadUsersFromFile();

            //Normalize email
            var aux = newUser.Email.Split(new char[] { '@' }, StringSplitOptions.RemoveEmptyEntries);

            var atIndex = aux[0].IndexOf("+", StringComparison.Ordinal);

            aux[0] = atIndex < 0 ? aux[0].Replace(".", "") : aux[0].Replace(".", "").Remove(atIndex);

            newUser.Email = string.Join("@", new string[] { aux[0], aux[1] });

            while (reader.Peek() >= 0)
            {
                var line = reader.ReadLineAsync().Result;
                var user = new User
                {
                    Name = line.Split(',')[0].ToString(),
                    Email = line.Split(',')[1].ToString(),
                    Phone = line.Split(',')[2].ToString(),
                    Address = line.Split(',')[3].ToString(),
                    UserType = line.Split(',')[4].ToString(),
                    Money = decimal.Parse(line.Split(',')[5].ToString()),
                };
                _users.Add(user);
            }
            reader.Close();
        }
        private decimal ChangeMoney(decimal money, string userType)
        {
            return (userType == "Normal" && money > 100) ? (money + (money * Convert.ToDecimal(0.12))) :
                   (userType == "Normal" && money > 10 && money < 100) ? (money + (money * Convert.ToDecimal(0.8))) :
                   (userType == "SuperUser" && money > 100) ? (money + (money * Convert.ToDecimal(0.20))) :
                   (userType == "Premium" && money > 100) ? (money + (money * 2)) : money;
        }

        //Validate errors
        private void ValidateErrors(string name, string email, string address, string phone, ref string errors)
        {
            if (name == null)
                //Validate if Name is null
                errors = "The name is required";
            if (email == null)
                //Validate if Email is null
                errors = errors + " The email is required";
            if (address == null)
                //Validate if Address is null
                errors = errors + " The address is required";
            if (phone == null)
                //Validate if Phone is null
                errors = errors + " The phone is required";
        }

    }
}
