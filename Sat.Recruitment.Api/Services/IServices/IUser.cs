﻿using Sat.Recruitment.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sat.Recruitment.Api.Services.IServices
{
    public interface IUser
    {
        Task<Result> CreateUser(User userParam);
    }
}
