﻿using Sat.Recruitment.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sat.Recruitment.Api.Services.IServices
{
    public interface IMoney
    {
        public decimal GetMoney(decimal money);
    }
}
